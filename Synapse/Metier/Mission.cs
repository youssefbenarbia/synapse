﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Metier;


namespace Synapse.Metier
{
    class Mission
    {
        private string _nom;
        private string _description;
        private decimal _nbHeuresPrevues;
        private Dictionary<DateTime, int> _releveHoraire;
        private Intervenant _executant;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public decimal NbHeurePrevues
        {
            get { return _nbHeuresPrevues; }
            set { _nbHeuresPrevues = value; }
        }

        public Dictionary<DateTime, int> ReleveHoraire
        {
            get { return _releveHoraire; }
            set { _releveHoraire = value; }
        }
        public Intervenant Executant
        {
            get { return _executant; }
            set { _executant = value; }
        }

        public int NbHeureEffectuees()
        {
            int _nbHeureEffectuees = 0;
            foreach (KeyValuePair<DateTime, int> kvp in _releveHoraire)
            {
                //Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                _nbHeureEffectuees+=kvp.Value;
            }
            return _nbHeureEffectuees;
        }

        internal object getExecutant()
        {
            throw new NotImplementedException();
        }
        public void ajouteReleve(DateTime date,int nbHeures)
        {
            this._releveHoraire.Add(date, nbHeures);
        }
    }
}
