﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Metier;

namespace Synapse.Metier
{
    class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMO;
        private List<Mission> _missions;
        
        public string Nom
        {
            get { return _nom;}
            set { _nom = value; }
        }

        public DateTime Debut
        {
            get { return _debut; }
            set { _debut = value; }
        }

        public DateTime Fin
        {
            get { return _fin; }
            set { _fin = value; }
        }

        public decimal PrixFactureMO
        {
            get { return _prixFactureMO; }
            set { _prixFactureMO = value; }
        }
        private List<Mission> Missions
        {
            get { return _missions; }
            set { _missions = value; }
        }
        
        private decimal  CumulCoutMO()
        {
            decimal cumul = 0;
            foreach (Mission m in _missions)
            {
                cumul += m.NbHeureEffectuees()*m.getExecutant().getTauxHoraire();
                //traitement décomposé : 
                //Intervernant i = m.getExecutant();
                //decimal taux = i.getTauxHoraire();
                //int nbHeures = m.nbHeuresEffectuees();
                //cumul+=taux*nbHeures;
            }
            return cumul;
        }

        public decimal margeBruteCourante()
        {
            decimal marge = 0;
            marge = this.PrixFactureMO - this.CumulCoutMO();
            return marge;
        }
            


    }
}
